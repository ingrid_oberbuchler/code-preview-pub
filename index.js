import classNames from 'classnames';
import { get as g, isEmpty } from 'lodash';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { setQuestionnaires } from 'academy/modules/academy/actions';
import { getAcademyNotificationsByCompId } from 'academy/modules/academy/selectors';
import { getLatestCompanyLink } from 'companies/modules/companies/selectors';
import { MENTION_TYPES } from 'global_modules/Notifications/constants';
import { makeGetNotificationsByCompany } from 'global_modules/Notifications/selectors';
import { getActiveCompany } from 'modules/Companies/selectors';
import Notice from 'modules/Notice';
import { getAcademyTeamByCompanyId } from 'modules/Teams/selectors';
import { getFirstUpperChar } from 'services/string';

import s from './index.scss';

class CompanyMenuItem extends Component {

  componentDidMount() {
    const { company: { _id: companyId }, isAcademyEnabled, setQuestionnaires } = this.props;
    if (isAcademyEnabled) {
      setQuestionnaires(companyId);
    }
  }

  render() {
    const { linkTo, company, activeCompanyId, notificationCount, mentionCount } = this.props;
    const { name, picture } = company;
    let notice = null;
    if (mentionCount > 0) {
      notice = <Notice.Component value={mentionCount} />;
    } else if (notificationCount > 0) {
      notice = <span className={s.NotificationCircle}/>;
    }
    // first letter in Company name
    const firstLetter = name && <span className={s.letter}>{getFirstUpperChar(name)}</span>;
    // load photo or first letter
    const element = picture ? <img src={picture}/> : firstLetter;
    let className = classNames({
      [s.CompanyAvatar]: true,
      [s.WithPicture]: picture
    });

    return (
      <li className={s.CompanyItem}>
        <NavLink
          to={linkTo}
          className={s.CompanyItem__link}
          activeClassName={s.active}
          isActive={() => g(company, '_id') === activeCompanyId}
        >
          <div className={className}>
            {element}
          </div>
          <div className={s.CompanyName}>
            {name}
          </div>
          {notice}
        </NavLink>
      </li>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { company } = ownProps;
  const getNotificationsByCompany = makeGetNotificationsByCompany();
  const activeCompanyId = g(getActiveCompany(state), '_id');
  const notifications = getNotificationsByCompany(state, company._id);
  let mentionCount = 0, notificationCount = 0;
  if (company._id !== activeCompanyId) {
    notificationCount = notifications.length;
    mentionCount = notifications.filter(n => MENTION_TYPES.includes(n.type)).length;
  }
  const academyTeam = getAcademyTeamByCompanyId(g(company, '_id'))(state);
  if (!isEmpty(academyTeam) && company._id !== activeCompanyId) {
    mentionCount += getAcademyNotificationsByCompId(state, company._id);
  }

  return {
    activeCompanyId,
    isAcademyEnabled: !!academyTeam,
    linkTo: getLatestCompanyLink(company)(state),
    mentionCount,
    notificationCount,
  };
};

const mapDispatchToProps = {
  setQuestionnaires,
};

CompanyMenuItem.propTypes = {
  activeCompanyId: PropTypes.string,
  company: PropTypes.object,
  isAcademyEnabled: PropTypes.bool.isRequired,
  linkTo: PropTypes.string.isRequired,
  mentionCount: PropTypes.number.isRequired,
  notificationCount: PropTypes.number.isRequired,
  setQuestionnaires: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(CompanyMenuItem);
